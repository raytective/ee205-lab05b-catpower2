///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"

double fromCatPower( double catPower ) {
   (void) catPower;
   return CATPOWER_IN_ANYTHING;  // Cats do no work
}

double toCatPower( double energy ) {
   (void) energy;
   return CATPOWER_IN_ANYTHING;

}

