///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp 
/// @version 1.0
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGasolineEqToJoule( double gasoline) {
   return gasoline / GAS_IN_A_JOULE;
}


double fromJouleToGasolineEq( double joule ) {
   return joule * GAS_IN_A_JOULE;
}

