###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Rachel Watanabe <rkwatana@hawaii.edu>
### @date 14_02_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC = g++
CFLAGS = -g -Wall -Wextra

TARGET = catPower

all: $(TARGET)

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp

foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp

gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp

catPower.o: catPower.cpp ev.h cat.h foe.h megaton.h gge.h joule.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o ev.o cat.o foe.o megaton.o gge.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o cat.o foe.o megaton.o gge.o

clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x | grep -q "Unknown toUnit x"	
	@./catPower 3.14 m j | grep -q "3.14 m is 1.31378E+16 j"
	@./catPower 3.14 g e | grep -q "3.14 g is 2.37728E+27 e"
	@./catPower 3.14 f m | grep -q "3.14 f is 7.50478E+08 m"
	@./catPower 3.14 c g | grep -q "3.14 c is 0 g"
	@./catPower 3.14 x f | grep -q "Unknown fromUnit x"
	@./catPower a b c d  | grep -q Usage:
	@./catPower          | grep -q Usage:
	@./catPower a        | grep -q Usage:
	@./catPower a b      | grep -q Usage:

	@echo "All tests pass"
