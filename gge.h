///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h 
/// @version 1.0
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

const double GAS_IN_A_JOULE = 1/(1.213e8);
const char GASOLINE_EQ      = 'g';

extern double fromGasolineEqToJoule( double gasoline) ;
extern double fromJouleToGasolineEq( double joule ) ;

