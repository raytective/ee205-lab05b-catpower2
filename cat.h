///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h 
/// @version 1.0
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

const double CATPOWER_IN_ANYTHING = 0;
const char CAT_POWER     = 'c';

extern double fromCatPower( double catPower ) ;
extern double toCatPower( double energy ) ;

